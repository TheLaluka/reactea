# ReacTea

## Setup

```bash
npm install
```

## Code & Debug

```bash
npm run web
```

## Deploy

```bash
npm run build-web
```
